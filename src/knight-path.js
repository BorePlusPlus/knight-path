const debug = require('debug')('knight-path:knight-path');
const parser = require('./line-parser');
const graph = require('./graph');

function possibleStepsFrom(parent) {
  return graph[parent].map((child) => ({ parent, node: child }));
}

function traverseGraph(from, to) {
  const visited = { [from]: null };
  const Q = [];
  Q.push(...possibleStepsFrom(from));

  while (Q.length) {
    const { node, parent } = Q.shift();
    if (node in visited) {
      continue; // eslint-disable-line no-continue
    }
    visited[node] = parent;
    if (node === to) {
      break;
    }
    Q.push(...possibleStepsFrom(node));
  }
  return visited;
}

function getPath(to, visited) {
  const path = [to];
  let current = to;
  while (visited[current]) {
    const parent = visited[current];
    path.unshift(parent);
    current = parent;
  }

  debug('Path: %j', path);
  return path.join(' ');
}

function getShortestPath(input) {
  const [from, to] = parser.parse(input);
  if (from === to) {
    return from;
  }
  if (graph[from].includes(to)) {
    return `${from} ${to}`;
  }

  const visited = traverseGraph(from, to);
  return getPath(to, visited);
}

module.exports = {
  getShortestPath,
};
