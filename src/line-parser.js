const debug = require('debug')('knight-path:parser');

const regExp = /^\s*([A-H][1-8])\s+([A-H][1-8])\s*$/;

function signalError() {
  throw Error('Invalid input! Please provide start and end position on board. Example: D4 A6');
}

function parse(line) {
  if (!line) {
    signalError();
  }

  const positions = regExp.exec(line.toUpperCase());
  debug(positions, line);
  if (!positions) {
    signalError();
  }
  return [positions[1], positions[2]];
}

module.exports = {
  parse,
};
