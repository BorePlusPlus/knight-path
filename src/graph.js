const debug = require('debug')('knight-path:graph');

const xLabels = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
const yLabels = [1, 2, 3, 4, 5, 6, 7, 8];
const moves = [[1, 2], [2, 1], [2, -1], [1, -2], [-1, -2], [-2, -1], [-2, 1], [-1, 2]];
const graph = {};

function coordinatesToLabel(x, y) {
  const xLabel = xLabels[x];
  const yLabel = yLabels[y];
  if (!xLabel || !yLabel) {
    return null;
  }
  return `${xLabel}${yLabel}`;
}

function getMovesFor(x, y) {
  return moves
    .map(([xMove, yMove]) => coordinatesToLabel(x + xMove, y + yMove))
    .filter((it) => it);
}

for (let x = 0; x < 8; x++) { // eslint-disable-line no-plusplus
  for (let y = 0; y < 8; y++) { // eslint-disable-line no-plusplus
    graph[coordinatesToLabel(x, y)] = getMovesFor(x, y);
  }
}

debug('graph %j', graph);
module.exports = graph;
