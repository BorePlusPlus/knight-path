Knight Path
===========
*Finding paths for lazy knights since 2018*

Installation
------------
Tool needs node.js (v8+) and npm (v5+) to install and run. Both are easily obtained by using
[nvm](https://github.com/creationix/nvm).

### Get node.js and npm
Execute this in directory where you checked out the repo.
```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
nvm install 8
nvm use
```

### Install dependencies
```
npm install
```

### Run tests
```
npm test
```

*Erm, that's it...*

Usage
-----
Tool has a simple command line interface. It accepts input in form of start and end location in usual notation. For example if user is interested in finding out the path from H1 to H5, they need to simply type `H1 H5` at the prompt. As tool is gentle on your shift key, it allows case insensitive input, so `h1 h5` would also be acceptable.

Tool is started by executing `npm start` and exited by sending SIGINT (Ctrl+C) or EOF (Ctrl+D).

Example session:
```
$ npm start

> knight-path@1.0.0 start /home/bore/projects/playground/knight-path
> node index.js

> D4 G7
D4 E6 G7
> D4 D5
D4 E6 F4 D5
> A1 H8
A1 B3 C5 D7 F8 G6 H8
> d4 d5
D4 E6 F4 D5
> Come again...
$
```

Notes
-----
As [knight can visit every square](https://en.wikipedia.org/wiki/Knight%27s_tour) on the board, there is no need to
check if a square is reachable from any other square.

Initial idea was to generate a graph of all valid knight moves and use [Dijkstra's algorithm](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm) to find the shortest path. But I concluded that the full implementation is not necessary as all the distances are same, so just a plain [Breadth-first search](https://en.wikipedia.org/wiki/Breadth-first_search) is sufficient (tell me if I'm wrong), we just need to keep parent nodes of visited nodes, so we can construct the path.

*Happy knighting - I hope it all works* :star2:
