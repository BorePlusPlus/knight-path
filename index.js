/* eslint-disable no-console */
const readline = require('readline');
const knightPath = require('./src/knight-path');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: '> ',
});

rl.prompt();

rl.on('line', (line) => {
  try {
    console.log(knightPath.getShortestPath(line));
  } catch (e) {
    console.log(`Error: ${e.message}`);
  }
  rl.prompt();
}).on('close', () => {
  console.log('Come again...');
});
