const { expect } = require('chai');
const graph = require('../src/graph');

describe('Graph of knight moves', () => {
  it('has 64 nodes', () => {
    expect(Object.keys(graph).length).to.equal(64);
  });

  it('names nodes A1 to H8', () => {
    ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'].forEach((x) => {
      [1, 2, 3, 4, 5, 6, 7, 8].forEach((y) => {
        expect(graph[`${x}${y}`]).to.exist; // eslint-disable-line no-unused-expressions
      });
    });
  });

  it('has correct edges for D4', () => {
    expect(graph.D4).to.eql(['E6', 'F5', 'F3', 'E2', 'C2', 'B3', 'B5', 'C6']);
  });

  it('has correct edges for H1', () => {
    expect(graph.H1).to.eql(['F2', 'G3']);
  });
});
