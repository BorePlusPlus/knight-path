const { expect } = require('chai');
const knightPath = require('../src/knight-path');

describe('Knight path', () => {
  it('throws error for illegal path', () => {
    expect(() => knightPath.getShortestPath('A0 F1')).to.throw();
  });

  it('find path from a field to itself', () => {
    expect(knightPath.getShortestPath('B3 B3')).to.equal('B3');
  });

  it('finds single step path', () => {
    expect(knightPath.getShortestPath('B3 D4')).to.equal('B3 D4');
  });

  [
    { input: 'D4 G7', path: 'D4 E6 G7' },
    { input: 'D4 D5', path: 'D4 E6 F4 D5' },
    { input: 'A1 H8', path: 'A1 B3 C5 D7 F8 G6 H8' },
  ].forEach(({ input, path }) => {
    it(`finds multi-step path (${input})`, () => {
      expect(knightPath.getShortestPath(input)).to.equal(path);
    });
  });
});
