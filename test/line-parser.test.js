const { expect } = require('chai');
const parser = require('../src/line-parser');

describe('Line parser', () => {
  it('returns invalid on empty line', () => {
    expect(() => parser.parse('')).to.throw(/Invalid input/);
  });

  [
    { line: 'D6 B2', out: ['D6', 'B2'] },
    { line: 'A1 H8', out: ['A1', 'H8'] },
    { line: 'A8 H1', out: ['A8', 'H1'] },
  ].forEach(({ line, out }) => {
    it(`parses valid line (${line})`, () => {
      expect(parser.parse(line)).to.eql(out);
    });
  });


  ['D0 B2', 'X1 H1', 'A-1 B4'].forEach((line) => {
    it(`return invalid on positions outside of board (${line})`, () => {
      expect(() => parser.parse(line)).to.throw(/Invalid input/);
    });
  });

  ['Lorem ipsum', '   ', 'A1B2C3', 'A1 B2 C3'].forEach((line) => {
    it(`return invalid on garbage input (${line}}`, () => {
      expect(() => parser.parse(line)).to.throw(/Invalid input/);
    });
  });

  it('strips spaces', () => {
    expect(parser.parse('  A1 D6    ')).to.eql(['A1', 'D6']);
  });

  it('allows for multiple spaces between positions', () => {
    expect(parser.parse('A1     D6')).to.eql(['A1', 'D6']);
  });

  it('allows for lower case characters', () => {
    expect(parser.parse('a1 d6')).to.eql(['A1', 'D6']);
  });
});
